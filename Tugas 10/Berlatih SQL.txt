Soal 1 Membuat Database
	CREATE DATABASE myshop;


Soal 2 Membuat Table di Dalam Database

	1 Table User
		CREATE TABLE users( 
		id int(8) PRIMARY KEY AUTO_INCREMENT, 
		name varchar(255) not null, 
		email varchar(255) not null, 
		password varchar(255) not null 
		);

	2 Table categories 
		CREATE TABLE categories( 
		id int(8) PRIMARY key AUTO_INCREMENT, 
		name varchar(255) not null 
		);

	3 Table items
		CREATE TABLE items( 
		id int PRIMARY KEY AUTO_INCREMENT, 
		name varchar(255) NOT null, 
		description varchar(255) NOT null, 
		price int(8) NOT null, 
		stock int(8) NOT null, 
		categories_id int(8), 
		FOREIGN KEY(categories_id) REFERENCES categories(id) 
		);


Soal 3 Memasukkan Data pada Table
	Table Users
		INSERT INTO users(name, email, password) VALUES ("John Doe", "john@doe.com", "john123")
		INSERT INTO users(name, email, password) VALUES ("Jane Doe", "jane@doe.com", "jenita123");

	Table Categories
		INSERT INTO categories(name) VALUES ("gadget"),("cloth"),("men"),("women"),("branded");

	Table Items
		INSERT INTO items(name, description, price, stock, categories_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1);
		INSERT INTO items(name, description, price, stock, categories_id) VALUES ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2);
		INSERT INTO items(name, description, price, stock, categories_id) VALUES ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


Soal 4 Mengambil Data dari Database

	a. Mengambil data users
		SELECT id, name, email FROM users;

	b. Mengambil data items
		- SELECT * FROM items WHERE price > 1000000;
		- SELECT * FROM items WHERE name LIKE "%watch";

	c. Menampilkan data items join dengan kategori
		-SELECT items.*, categories.name from items INNER JOIN categories ON items.categories_id = categories.id;

Soal 5 Mengubah Data dari Database

		- UPDATE items SET price=2500000 WHERE id=1;

