<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/master', function () {
    return view('layout.master');
});
Route::get('/data-tables', function () {
    return view('page.data-table');
});
Route::get('/table', function () {
    return view('page.table');
});

//CRUD

//create data
Route::get('/cast/create',[CastController::class,'create']);
route::post('/cast', [CastController::class, 'store']);

//Read Data
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{id}', [CastController::class, 'show']);

//Update Data
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{id}', [CastController::class, 'update']);

//Delete Data
Route::delete('/cast/{id}', [CastController::class, 'destroy']);