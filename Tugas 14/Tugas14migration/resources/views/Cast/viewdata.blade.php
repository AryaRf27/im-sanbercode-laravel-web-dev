@extends('layout.master')

@section('title')
    Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-sm btn-primary">Tambah</a>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value)
         <tr>
             <th scope="row">{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                    <td>
                       
                        <form action="cast/{{$value->id}}" method="POST">
                            @csrf
                            @method('delete')
                            <a class="btn btn-info btn-sm>" href="/cast/{{$value->id}}">Detail</a>
                            <a class="btn btn-warning btn-sm" href="/cast/{{$value->id}}/edit">Edit</a>
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    
                    </td>
                </td>
                @empty
                    <tr>
                        <th>Tidak Ada Cast</th>
                    </tr>
            </td>
        @endforelse
    </tbody>
  </table>

@endsection