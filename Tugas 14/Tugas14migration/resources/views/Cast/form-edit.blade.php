@extends('layout.master')

@section('title')
    Edit Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('PUT')
    <div class="form-group">
      <label>Nama</label>
      <input type="string" name="nama" value="{{$cast->nama}}" class="@error('name') is-invalid @enderror form-control" >
    </div>
      @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="number" name="umur" value="{{$cast->umur}} class="@error('name') is-invalid @enderror form-control form-control">
    </div>
      @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
        <label>Bio</label>
        <textarea name='bio' class="@error('name') is-invalid @enderror form-control form-control" cols="30" rows="10" {{$cast->bio}}></textarea>
      </div>
      @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection