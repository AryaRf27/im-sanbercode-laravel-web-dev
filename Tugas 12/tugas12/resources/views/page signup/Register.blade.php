<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="/SignUp" method="POST">
        @csrf
        <label>First Name</label><br><br>
        <input type="text" name="fname"><br><br>
        <label>Last Name</label><br><br>
        <input type="text" name="lname"><br><br>
        <label>Gender</label><br><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">other<br><br>
        <label>Nationality</label>
        <br><br>
        <select name="Country">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapore">Singapore</option>
            </select>
            <br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="Language">Bahasa Indonesia<br>
        <input type="checkbox" name="Language">English<br>
        <input type="checkbox" name="Language">Other<br>
        <label>Bio</label><br>
        <textarea name="message" rows="10" cols="30"></textarea>
        <br>
        <nav>
            <input type="submit" value="SignUp"></a>|
        </nav>
    </form>

</body>
</html>