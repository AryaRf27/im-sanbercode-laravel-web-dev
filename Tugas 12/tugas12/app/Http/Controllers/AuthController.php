<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('page signup.Register');
    }

    public function send(Request $request){
        $FirstName= $request->input('fname');
        $LastName= $request->input('lname');

        return view('page signup.succes', ["FirstName"=>$FirstName,"LastName"=>$LastName]);
    }
}
